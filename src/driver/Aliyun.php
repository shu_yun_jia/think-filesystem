<?php

declare(strict_types=1);

namespace hulang\filesystem\driver;

use League\Flysystem\AdapterInterface;
use hulang\filesystem\traits\Storage;
use think\filesystem\Driver;
use Xxtime\Flysystem\Aliyun\OssAdapter;

class Aliyun extends Driver
{
    use Storage;

    protected function createAdapter(): AdapterInterface
    {
        $aliyun = new OssAdapter([
            'accessId'     => $this->config['accessId'],
            'accessSecret' => $this->config['accessSecret'],
            'bucket'       => $this->config['bucket'],
            'endpoint'     => $this->config['endpoint'],
        ]);

        return $aliyun;
    }
}
