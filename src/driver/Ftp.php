<?php

declare(strict_types=1);

namespace hulang\filesystem\driver;

use League\Flysystem\AdapterInterface;
use League\Flysystem\Ftp\FtpAdapter;
use League\Flysystem\FTP\FtpConnectionOptions;
use hulang\filesystem\traits\Storage;
use think\filesystem\Driver;

class Ftp extends Driver
{
    use Storage;

    protected function createAdapter(): AdapterInterface
    {
        $adapter = new FtpAdapter(
            // Connection options
            FtpConnectionOptions::fromArray([
                'host'       => $this->config['host'],
                'root'       => $this->config['root'],
                'username'   => $this->config['username'],
                'password'   => $this->config['password'],
            ]),
            new \League\Flysystem\FTP\FtpConnectionProvider(),
            new \League\Flysystem\FTP\NoopCommandConnectivityChecker(),
            new \League\Flysystem\UnixVisibility\PortableVisibilityConverter(),
        );

        return $adapter;
    }
}
