<?php

declare(strict_types=1);

namespace hulang\filesystem\driver;

use League\Flysystem\AdapterInterface;
use League\Flysystem\Sftp\SftpAdapter;
use League\Flysystem\Filesystem;
use hulang\filesystem\traits\Storage;
use think\filesystem\Driver;

class Sftp extends Driver
{
    use Storage;

    protected function createAdapter(): AdapterInterface
    {
        $adapter = new SftpAdapter([
            'host'           => $this->config['host'],
            'port'           => $this->config['port'] ?? 22,
            'username'       => $this->config['username'],
            'password'       => $this->config['password'],
            'privateKey'     => $this->config['privateKey'],
            'passphrase'     => $this->config['passphrase'],
            'root'           => $this->config['root'],
            'timeout'        => $this->config['timeout'] ?? 10,
            'directoryPerm'  => $this->config['directoryPerm'] ?? 0755,
        ]);

        return $adapter;
    }
}
